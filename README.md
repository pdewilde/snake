# Snake

Snake implemented in lots of different languages, written for the YouTube
series "[First Impressions (formed by writing snake)](https://www.youtube.com/watch?v=LZzr9wvlWaE&list=PLgyU3jNA6VjTYOJZVTYuxMz2UD7hi4Xsu)"
in which I write Snake in different languages and technologies, and use the
experience to form first impressions of them.

The source code of the slides is at https://gitlab.com/andybalaam/videos-snake .

## License

Copyright Andy Balaam.  Released under the GPLv2 or later.  See
[LICENSE.md](LICENSE.md) for more info.
