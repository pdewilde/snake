variable "inp" {
    type = string
    default = "l"
}

variable "size" {
    type = number
    default = 10
}



resource "random_integer" "apple" {
    min = 1
    max = var.size - 1
    count = 2
}

locals {
    # in_model = {
    #     size = var.size
    #     snake = [[4, 5], [4, 6], [4, 7], [4, 8]]
    #     apple = [random_integer.apple[0].result, random_integer.apple[1].result]
    #     dir = "u"
    #     dead = false
    # }
    in_model = "${jsondecode(data.local_file.load_state.content)}"
}

data local_file "load_state" {
    filename = "state.json"
}

resource local_file "store_state" {
    content = "${jsonencode(module.update_one.value)}"
    filename = "state.json"
}

module "update_one" {
    source = "./modules/update_one"
    model = local.in_model
    inp = var.inp
    idx = 0
}

# module "update" {
#     source = "./modules/update"
#     model = local.initial_model
#     inp = var.inp
# }

module "view" {
    source = "./modules/view"
    model = module.update_one.value
}

output "text" {
    value = module.view.text
}
