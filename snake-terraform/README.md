# Snake in Terraform

A game of Snake that can teach you patience, and warm your working environment.

## Prerequisites

I cheated, so we need:

* Python3 and PyGame (`sudo apt install python3-pygame`)

as well as:

* [Terraform](https://www.terraform.io/downloads.html)

## Run

```bash
terraform init
rm -f terraform.tfstate
./play
```

## Test
```bash
terraform init
rm -f terraform.tfstate
./test
```
