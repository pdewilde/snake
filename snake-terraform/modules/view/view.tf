variable "model" {
}

locals {
    xs = range(var.model.size)
    ys = range(var.model.size)

    grid = [for y in local.ys: [for x in local.xs:
        (
            var.model.dead &&
            [x, y] == var.model.snake[0]
        ) ?
            "d" :
            (
                x == 0
                || x == var.model.size - 1
                || y == 0
                || y == var.model.size - 1
            ) ?
            "#" :
                (x == var.model.apple[0] && y == var.model.apple[1]) ?
                    "a" :
                    (contains(var.model.snake, [x, y])) ?
                        "o" :
                        " "
    ]]

    lst = [for chars in local.grid: join("", chars)]
    str = join("\n", local.lst)
}

output "text" {
    value = local.str
}
