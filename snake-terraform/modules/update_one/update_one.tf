variable "model" {
}

variable "inp" {
    type = string
}

variable "idx" {
    type = number
}

resource "random_integer" "apple" {
    min = 1
    max = var.model.size - 1
    count = 2
}

locals {
    new_dir = substr(var.inp, var.idx, 1)

    dir = (
        local.new_dir == "" || var.model.dead ?
            "X" :
            local.new_dir == "." ?
                var.model.dir :
                local.new_dir
    )

    move = ((local.dir == "l") ?
        [-1, 0] :
        (local.dir == "r") ?
            [1, 0] :
            (local.dir == "u") ?
                [0, -1] :
                (local.dir == "d") ?
                    [0, 1] :
                    [0, 0])

    new_head = [
        var.model.snake[0][0] + local.move[0],
        var.model.snake[0][1] + local.move[1]
    ]

    dead = (
        var.model.dead ||
        (
            contains(var.model.snake, local.new_head) ||
                local.new_head[0] < 1 ||
                local.new_head[0] >= var.model.size - 1 ||
                local.new_head[1] < 1 ||
                local.new_head[1] >= var.model.size - 1
        )
    )

    eat = (local.new_head == var.model.apple)

    next_model = (
        local.dir == "X" ?
            var.model :
            {
                size = var.model.size
                snake = (
                        local.eat ?
                            concat(
                                [local.new_head],
                                slice(
                                    var.model.snake,
                                    0,
                                    length(var.model.snake) - 1
                                ),
                                [for _ in range(5): [-1, -1]]
                            ) :
                            concat(
                                [local.new_head],
                                slice(
                                    var.model.snake,
                                    0,
                                    length(var.model.snake) - 1
                                )
                            )
                )
                dir = local.dir
                apple = (
                    local.eat ?
                        [
                            random_integer.apple[0].result,
                            random_integer.apple[1].result
                        ]:
                        var.model.apple
                )
                dead = local.dead
            }
    )
}

output "value" {
    value = local.next_model
}
